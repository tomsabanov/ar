using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCvSharp;
using UnityEngine.UI;
using System.Runtime.InteropServices;
using NWH;

public class WebCamera : MonoBehaviour
{
    public RawImage rawImage;
    public AspectRatioFitter fit;

    int MIN_MATCHES = 40;
    int MIN_DISTANCE = 50;
    int SCALE = 3;
    int NUM_OF_KEYPOINTS = 1000;
    int f = 550;
    
    

    private WebCamTexture webCamTexture;
    private bool initialized = false;
    private int camHeight;
    private int camWidth;

    // Image rotation
    Vector3 rotationVector = new Vector3(0f, 0f, 0f);
    // Image uvRect
    UnityEngine.Rect defaultRect = new UnityEngine.Rect(0f, 0f, 1f, 1f);
    UnityEngine.Rect fixedRect = new UnityEngine.Rect(0f, 1f, 1f, -1f);
    // Image Parent's scale
    Vector3 defaultScale = new Vector3(1f, 1f, 1f);
    Vector3 fixedScale = new Vector3(-1f, 1f, 1f);

    private GameObject model;

    private GameObject currentObject;
    Texture2D camTexture;
    Mat camMat;
    void Start()
    {
        model = new GameObject();
        ObjectLoader loader = model.AddComponent<ObjectLoader>();
        loader.Load("Assets/Fox/", "low-poly-fox-by-pixelmannen.obj");
        model.transform.localPosition = new Vector3(0, 0, 0);
        model.SetActive(false);
        model.transform.parent = rawImage.transform;
        model.transform.localScale = new Vector3(1, 1, 1);

        currentObject = model;


        CvUtil.CheckIfCameraExists();

        prepareCard();
        webCamTexture = new WebCamTexture(WebCamTexture.devices[1].name);
        //webCamTexture.filterMode = FilterMode.Trilinear;

        webCamTexture.Play();
        rawImage.texture = webCamTexture;
    }

    void Update()
    {
        // Rotate image to show correct orientation 
        rotationVector.z = -webCamTexture.videoRotationAngle;
        rawImage.rectTransform.localEulerAngles = rotationVector;

        // Set AspectRatioFitter's ratio
        float videoRatio =(float)webCamTexture.width / (float)webCamTexture.height;
        fit.aspectRatio = videoRatio;

        // Unflip if vertically flipped
        rawImage.uvRect = webCamTexture.videoVerticallyMirrored ? fixedRect : defaultRect;
        // Wait for camera to return the first frame and then grab the correct width and height.

        
        if (!initialized && CvUtil.CameraReturnedFirstFrame(webCamTexture))
        {
            camHeight = webCamTexture.height;
            camWidth = webCamTexture.width;
            camTexture = new Texture2D(camWidth, camHeight, TextureFormat.RGB24, false);
            camMat = new Mat(camHeight, camWidth, MatType.CV_8UC4);
            Debug.Log("Cam height is " + camHeight);
            Debug.Log("Cam width is " + camWidth);
            initialized = true;
        }
        else if (webCamTexture != null && webCamTexture.didUpdateThisFrame && webCamTexture.isPlaying)
        {
            Boolean isSet = CamUpdate();
            if (isSet)
            {
                currentObject.SetActive(true);
            }
            else
            {
                currentObject.SetActive(false);
            }
        }

        
    }
    

    (KeyPoint[], Mat) prepareCamera()
    {
        // returns camera keypoints, descriptors, cameraMat

        CvUtil.GetWebCamMat(webCamTexture, ref camMat);
        Cv2.CvtColor(camMat, camMat, ColorConversionCodes.RGBA2RGB); 

        Mat gray = new Mat();
        Cv2.CvtColor(camMat, gray, ColorConversionCodes.BGR2GRAY);

        OpenCvSharp.ORB orb = ORB.Create(NUM_OF_KEYPOINTS);
        KeyPoint[] pictureKP = orb.Detect(gray, null);
        Mat camDescriptors = new Mat();
        orb.Compute(gray, ref pictureKP, camDescriptors);

        return (pictureKP, camDescriptors);

    }
    int index = 0;
    Boolean CamUpdate()
    {
        // Get Keypoints and Descripots of camera footage
        var cameraData = prepareCamera();
        KeyPoint[] cameraKP = cameraData.Item1;
        Mat cameraDescriptor = cameraData.Item2;
  
        // Check KeyPoints in card
        var  matchesData = featureMatching(cameraDescriptor);
        if (matchesData.Item1 == null || matchesData.Item2 == null)
        {
            rawImage.texture = webCamTexture;
            return false;
        }
        
        DMatch[] matches = matchesData.Item1;
        DMatch[] accMatches = matchesData.Item2;

        //writeMatches(camMat, cameraKP, accMatches);


        var data = Homography(cameraKP,matches); 
        
        
        Mat H = data.Item1;
        Point2d[] corners = data.Item2;

        if (H == null) return false;

        var proj = ProjectionMatrix(H);
        Mat projection = proj.Item1;
        Mat R = proj.Item2;

        if(index%10 == 0)
        {
            placeModel(projection, corners, R);
        }
        //drawModel(projection);

        CvConvert.MatToTexture2D(camMat, ref camTexture);
        rawImage.texture = camTexture;
        index++;

        return true;
    }
    Point projectVector(Mat projection,Vector3 V)
    {
        double x = 0;
        double y = 0;
        double t = 0;
        x += projection.Get<double>(0, 0) * V.x;
        x += projection.Get<double>(0, 1) * V.y;
        x += projection.Get<double>(0, 2) * V.z;
        x += projection.Get<double>(0, 3);

        y += projection.Get<double>(1, 0) * V.x;
        y += projection.Get<double>(1, 1) * V.y;
        y += projection.Get<double>(1, 2) * V.z;
        y += projection.Get<double>(1, 3);

        t += projection.Get<double>(2, 0) * V.x;
        t += projection.Get<double>(2, 1) * V.y;
        t += projection.Get<double>(2, 2) * V.z;
        t += projection.Get<double>(2, 3);

        x = x / t;
        y = y / t;

        Point p = new Point((int)x, (int)y);
        return p;
    }
    Vector3 project3DVector(Mat projection, Vector3 V)
    {
        double x = 0;
        double y = 0;
        double t = 0;
        x += projection.Get<double>(0, 0) * V.x;
        x += projection.Get<double>(0, 1) * V.y;
        x += projection.Get<double>(0, 2) * V.z;
        x += projection.Get<double>(0, 3);

        y += projection.Get<double>(1, 0) * V.x;
        y += projection.Get<double>(1, 1) * V.y;
        y += projection.Get<double>(1, 2) * V.z;
        y += projection.Get<double>(1, 3);

        t += projection.Get<double>(2, 0) * V.x;
        t += projection.Get<double>(2, 1) * V.y;
        t += projection.Get<double>(2, 2) * V.z;
        t += projection.Get<double>(2, 3);

        x = x / t;
        y = y / t;

        Vector3 p = new Vector3((float)(x/t), (float)(y/t), -50);
        return p;
    }
    Vector3 getEuler(Mat R)
    {
        double sy = Math.Sqrt(R.At<double>(0, 0) * R.At<double>(0, 0));
        bool singular = sy < 1e-6;

        float x;
        float y;
        float z;

        if (!singular)
        {
            x = (float)Math.Atan2(R.At<double>(2, 1), R.At<double>(2, 2));
            y = (float)Math.Atan2(-R.At<double>(2, 0), sy);
            z = (float)Math.Atan2(R.At<double>(1, 0), R.At<double>(0, 0));
        }
        else
        {
            x = (float)Math.Atan2(-R.At<double>(1, 2), R.At<double>(1, 1));
            y = (float)Math.Atan2(-R.At<double>(2, 0), sy);
            z = 0;
        }
        x = (float)(x / 2 * Math.PI) * 360;
        y = (float)(y / 2 * Math.PI) * 360;
        z = (float)(z / 2 * Math.PI) * 360;

        return new Vector3(x, y, z);
    }
    void drawModel(Mat projection)
    {
        Mesh mesh = currentObject.GetComponent<MeshFilter>().mesh;

        Vector3[] vertices = mesh.vertices;
        int[] indices = mesh.triangles;
        int triangleCount = indices.Length / 3;

      
        int w = cardMat.Width;
        int h = cardMat.Height;
        for (int i = 0; i < triangleCount;  i++)
        {

            Vector3 V1 = vertices[indices[i * 3]];
            Vector3 V2 = vertices[indices[i * 3 + 1]];
            Vector3 V3 = vertices[indices[i * 3 + 2]];

            V1.x = V1.x + w/2;
            V1.y = V1.y + h/2;

            V2.x = V2.x + w/2;
            V2.y = V2.y + h/2;

            V3.x = V3.x + w/2;
            V3.y = V3.y + h/2;
            // projection matrix is 3*4
            // we  must multiply each row of the projection matrix 
            // with the extended Vector3 vertice [x y z 1]
            // then we get [u v t] vector, where u,v represent x,y scaled by t

            Point p1 = projectVector(projection,V1);
            Point p2 = projectVector(projection, V2);
            Point p3 = projectVector(projection, V3);

            Point[] dst = new Point[3];
            dst[0] = p1;
            dst[1] = p2;
            dst[2] = p3;

            Cv2.FillConvexPoly(camMat, dst, 120);
        }
       //CvConvert.MatToTexture2D(camMat, ref camTexture);
        //rawImage.texture = camTexture;
    }

    void placeModel(Mat projection, Point2d[] corners,Mat R)
    {
        Vector3 rotation = getEuler(R);
        currentObject.transform.localEulerAngles = rotation;

        Vector3 position = project3DVector(projection, currentObject.transform.position);
        currentObject.transform.localPosition = position;

    }

    void PrintMatrix(Mat M)
    {
        for (int i = 0; i < M.Rows; i++)
        {
            for (int j = 0; j < M.Cols; j++)
            {
                Debug.Log(M.At<double>(i, j).ToString() + " value at " + i + "," + j);
            }
        }

    }

    Mat multByScalar(Mat M, double s)
    {
        for (int i = 0; i < M.Rows; i++)
        {
            for (int j = 0; j < M.Cols; j++)
            {
                M.Set<double>(i, j, s * M.Get<double>(i, j));
            }
        }
        return M;
    }
    Mat divideByScalar(Mat M , double s)
    {
        for (int i = 0; i < M.Rows; i++)
        {
            for (int j = 0; j < M.Cols; j++)
            {
                M.Set<double>(i, j, M.Get<double>(i, j)/s);
            }
        }
        return M;
    }
    Mat addMatrices(Mat M, Mat N)
    {
        Mat K = new Mat(M.Rows, M.Cols, MatType.CV_64FC1);
        for (int i = 0; i < M.Rows; i++)
        {
            for (int j = 0; j < M.Cols; j++)
            {
                K.Set<double>(i, j, M.Get<double>(i, j) + N.Get<double>(i, j));
            }
        }
        return K;
    }
    Mat subtractMatrices(Mat M, Mat N)
    {
        Mat K = new Mat(M.Rows, M.Cols, MatType.CV_64FC1);
        for (int i = 0; i < M.Rows; i++)
        {
            for (int j = 0; j < M.Cols; j++)
            {
                K.Set<double>(i, j, M.Get<double>(i, j) - N.Get<double>(i, j));
            }
        }
        return K;
    }
    (Mat,Mat) ProjectionMatrix(Mat H)
    {
        Mat cameraParameters = new Mat(3, 3, MatType.CV_64FC1);
        cameraParameters.Set<double>(0, 0, f);
        cameraParameters.Set<double>(0, 1, 0);
        cameraParameters.Set<double>(0, 2, camWidth/2);
        cameraParameters.Set<double>(1, 0, 0);
        cameraParameters.Set<double>(1, 1, f);
        cameraParameters.Set<double>(1, 2, camHeight/2);
        cameraParameters.Set<double>(2, 0, 0);
        cameraParameters.Set<double>(2, 1, 0);
        cameraParameters.Set<double>(2, 2, 1);

        H = multByScalar(H, -1);

        Mat inv = cameraParameters.Inv(DecompTypes.SVD);

        Mat rot_and_transl = inv * H;

        Mat col_1 = rot_and_transl.ColRange(0, 1);
        Mat col_2 = rot_and_transl.ColRange(1, 2);
        Mat col_3 = rot_and_transl.ColRange(2, 3);

        double l = Math.Sqrt(col_1.Norm(NormTypes.L1) * col_2.Norm(NormTypes.L1));
        
        Mat rot_1 = divideByScalar(col_1,l);
        Mat rot_2 = divideByScalar(col_2, l);
        Mat translation = divideByScalar(col_3, l);

        Mat c = addMatrices(rot_1, rot_2);
        Mat p = rot_1.Cross(rot_2);
        Mat d = c.Cross(p);

        Mat tmp = divideByScalar(c, c.Norm(NormTypes.L1));
        Mat fact = divideByScalar(d, d.Norm(NormTypes.L1));

        rot_1 = multByScalar(addMatrices(tmp, fact), (1 / Math.Sqrt(2)));
        rot_2 = multByScalar(subtractMatrices(tmp, fact), (1 / Math.Sqrt(2)));

        Mat rot_3 = rot_1.Cross(rot_2);

        Mat projection = new Mat(3, 4, MatType.CV_64FC1);
        Mat R = new Mat(3, 3, MatType.CV_64FC1);
        for (int i = 0; i < 3; i++)
        {
            projection.Set<double>(i, 0, rot_1.Get<double>(i));
            R.Set<double>(i, 0, rot_1.Get<double>(i));
        }
        for (int i = 0; i < 3; i++)
        {
            projection.Set<double>(i, 1, rot_2.Get<double>(i));
            R.Set<double>(i, 1, rot_2.Get<double>(i));
        }
        for (int i = 0; i < 3; i++)
        {
            projection.Set<double>(i, 2, rot_3.Get<double>(i));
            R.Set<double>(i, 2, rot_3.Get<double>(i));
        }
        for (int i = 0; i < 3; i++)
        {
            projection.Set<double>(i, 3, translation.Get<double>(i));
        }
   
        return (cameraParameters * projection,R);
    }

    void writeMatches(Mat camMat, KeyPoint[] camKP,DMatch[] matches)
    {
        Mat output = new Mat();
        Cv2.DrawMatches(camMat, camKP, cardMat, cardKP, matches, output);

        System.Random rnd = new System.Random();
        int i = rnd.Next(1, 1000);
        string FileName = "C:\\Users\\Tom\\Desktop\\Pictures\\Output" + i.ToString() + ".jpg";
        output.ImWrite(FileName);
    }

    (DMatch[],DMatch[]) featureMatching(Mat camDescriptors)
    {
        if (camDescriptors == null || cardDescriptor == null) return (null, null);
        
        BFMatcher bf = new BFMatcher(NormTypes.Hamming, true);
        //Match frame descriptors with model descriptors

        DMatch[] matches = bf.Match(cardDescriptor,camDescriptors);

        //Sort them in the order of their distance
        Array.Sort(matches, delegate (DMatch match1, DMatch match2) {
            return match1.Distance.CompareTo(match2.Distance);
        });

     
        if (matches.Length < MIN_MATCHES) return (null, null);

        DMatch[] m = new DMatch[MIN_MATCHES];
        int inserted = 0;
        double min_distance = matches[0].Distance;
        double max_distance = matches[0].Distance;
        for(int i = 0; i< MIN_MATCHES; i++)
        {
            double dist = matches[i].Distance;

            if (dist > max_distance) max_distance = dist;
            if (dist < min_distance) min_distance = dist;
            if(matches[i].Distance <= MIN_DISTANCE)
            {
                m[i] = matches[i];
                inserted++;
            }
        }
  
        if (inserted == MIN_MATCHES) return (matches, m); ;
        return (null, null);
      
    }

    (Mat, Point2d[]) Homography(KeyPoint[] cameraKP, DMatch[] matches)
    {
        List<Point2f> src = new List<Point2f>();
        List<Point2f> dest = new List<Point2f>();
        for (int i = 0; i<MIN_MATCHES; i++)
        {
            src.Add(cardKP[matches[i].QueryIdx].Pt);
            dest.Add(cameraKP[matches[i].TrainIdx].Pt);
        }

        List<Point2d> objPts = src.ConvertAll(Point2fToPoint2d);
        List<Point2d> scenePts = dest.ConvertAll(Point2fToPoint2d);
        Mat H = Cv2.FindHomography(objPts, scenePts, HomographyMethods.Ransac, 5.0, null);

        if(H != null)
        {
            int h = cardMat.Height;
            int w = cardMat.Width;
            List<Point2d> pts = new List<Point2d>();
            pts.Add(new Point2d(0, 0));
            pts.Add(new Point2d(0, h - 1));
            pts.Add(new Point2d(w - 1, h - 1));
            pts.Add(new Point2d(w - 1, 0));

            Point2d[] sceneCorners = Cv2.PerspectiveTransform(pts, H);

            List<List<Point>> listOfListOfPoint2D = new List<List<Point>>();
            List<Point> listOfPoint2D = new List<Point>();
            listOfPoint2D.Add(new Point(sceneCorners[0].X , sceneCorners[0].Y));
            listOfPoint2D.Add(new Point(sceneCorners[1].X , sceneCorners[1].Y));
            listOfPoint2D.Add(new Point(sceneCorners[2].X , sceneCorners[2].Y));
            listOfPoint2D.Add(new Point(sceneCorners[3].X , sceneCorners[3].Y));
            listOfListOfPoint2D.Add(listOfPoint2D);

            Cv2.Polylines(camMat, listOfListOfPoint2D, true, 255, 3);
            return (H, sceneCorners);
        }
        rawImage.texture = webCamTexture;
        return (null, null);
    }

    public Point2d Point2fToPoint2d(Point2f pf) => new Point2d(((double)pf.X), ((double)pf.Y));
 
    private Texture2D cardTexture;
    private Mat cardMat;
    private KeyPoint[] cardKP;
    private Mat cardDescriptor;
    public TextAsset card;
    void prepareCard()
    {
        cardTexture = new Texture2D(1, 1, TextureFormat.RGB24, false);
        cardTexture.LoadImage(card.bytes);

        cardMat = new Mat(cardTexture.height, cardTexture.width, MatType.CV_8UC3);
        CvConvert.Texture2DToMat(cardTexture, ref cardMat);

        Mat gray = new Mat();
        Cv2.CvtColor(cardMat, gray, ColorConversionCodes.BGR2GRAY);        

        OpenCvSharp.ORB orb = ORB.Create(NUM_OF_KEYPOINTS);

        cardKP = orb.Detect(gray, null);
        cardDescriptor = new Mat();
        orb.Compute(gray, ref cardKP, cardDescriptor);

        //Cv2.DrawKeypoints(cardMat, cardKP, cardMat, new Scalar(0, 255, 0), DrawMatchesFlags.Default);
        //CvConvert.MatToTexture2D(cardMat, ref cardTexture);

    }

    private void OnDestroy()
    {
        if (webCamTexture != null) webCamTexture.Stop();
    }
}